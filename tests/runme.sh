#!/bin/bash
set -x

# https://projects.engineering.redhat.com/browse/PLATFORMCI-2495

# This is a workaround for opa-ff OSCI gating test. Until OSCI
# team resolves the ticket PLATFORMCI-2495, we can't run opa-ff
# beaker case over rdma-qe-14/15.

exit 0
